# python test app

## frontend

```url
/static/index.html
```

## RUN

```shell
docker run --cpus="MAX_CPU" -d --name fibonacci_container -p 80:80 fibonacci_image
```
