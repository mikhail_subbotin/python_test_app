from fastapi.testclient import TestClient
from main import app

client = TestClient(app)


def test_fib_10():
    response = client.post(
        "/fib",
        json={"number": "10"},
    )
    assert response.status_code == 200
    assert response.json() == {"result": "144"}


def test_fib_0():
    response = client.post(
        "/fib",
        json={"number": "0"},
    )
    assert response.status_code == 200
    assert response.json() == {"result": "1"}


def test_fib_empty_request():
    response = client.post(
        "/fib",
    )
    assert response.status_code == 422
