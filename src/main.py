from functools import lru_cache

from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles
from pydantic import BaseModel

app = FastAPI(
    title="FibonacciApp",
    description="API for calculating the n-th Fibonacci number",
    summary="Calculates the n-th Fibonacci number",
    version="0.0.1",
    license_info={
        "name": "Apache 2.0",
        "url": "https://www.apache.org/licenses/LICENSE-2.0.html",
    },
)
app.mount("/static", StaticFiles(directory="static"), name="static")


class FibonacciNumberRequest(BaseModel):
    number: int


@lru_cache()
async def fibonacci_calculation(n):
    a, b = 0, 1
    while n >= 0:
        a, b = b, a + b
        n -= 1
    return b


@app.post("/fib")
async def fib(fibonacci_index: FibonacciNumberRequest):
    """
    Calculate fibonacci number by fibonacci_index  (use only last two elements to calculate)
    """
    fibonacci_value = await fibonacci_calculation(fibonacci_index.number)
    # return string to avoid exp notation
    return {"result": f"{fibonacci_value}"}



